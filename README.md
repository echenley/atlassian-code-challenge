# Atlassian Code Challenge

I included the production build in the repo so you don't have to install dependencies. Run `npm start` to start the server and then open up `localhost:3000`.

Try the arrow keys. Thanks for reviewing!

-Evan
