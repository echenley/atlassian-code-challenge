'use strict';

var path = require('path');
var express = require('express');

var app = express();

app.use(express.static(path.join(__dirname, 'dist')));
app.use('/static', express.static(path.join(__dirname, 'static')));

app.listen(3000, 'localhost', function(err) {
    if (err) {
        console.log(err);
    }

    console.log('Listening at localhost:3000');
});
