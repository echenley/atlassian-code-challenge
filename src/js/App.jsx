'use strict';

import React, {
    PropTypes,
    Component
} from 'react';

import Radium, { Style } from 'radium';
import { globalStyles, container } from './styles/styles';

import Header from './components/Header';
import Footer from './components/Footer';
import Tabs from './components/Tabs';
import Sidebar from './components/Sidebar';
import Article from './components/Article';
import SummitLogo from './components/SummitLogo';
import sessionModel from './models/sessionModel';

const keyCodes = {
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40
};

class App extends Component {
    static propTypes = {
        tracks: PropTypes.objectOf(
            PropTypes.arrayOf(sessionModel)
        ).isRequired
    };

    constructor(props) {
        super(props);

        let trackTypes = Object.keys(props.tracks);
        let activeTrack = trackTypes[0];
        let activeSession = props.tracks[activeTrack][0];

        this.state = {
            trackTypes,
            activeTrack,
            activeSession
        };
    }

    componentDidMount() {
        document.addEventListener('keydown', (e) => this.handleKeyPress(e));
    }

    handleKeyPress(e) {
        const { tracks } = this.props;
        const { trackTypes, activeTrack, activeSession } = this.state;
        const { LEFT, UP, DOWN, RIGHT } = keyCodes;

        const activeTrackIndex = trackTypes.indexOf(activeTrack);
        const activeSessionIndex = tracks[activeTrack].indexOf(activeSession);

        if (e.keyCode > 40 || e.keyCode < 37 || e.metaKey || e.ctrlKey) {
            return;
        }

        e.preventDefault();

        if (e.keyCode === LEFT && activeTrackIndex > 0) {
            let newActiveTrack = trackTypes[activeTrackIndex - 1];
            this.setState({
                activeTrack: newActiveTrack,
                activeSession: tracks[newActiveTrack][0]
            });
        } else if (e.keyCode === RIGHT && activeTrackIndex < trackTypes.length - 1) {
            let newActiveTrack = trackTypes[activeTrackIndex + 1];
            this.setState({
                activeTrack: newActiveTrack,
                activeSession: tracks[newActiveTrack][0]
            });
        } else if (e.keyCode === UP && activeSessionIndex > 0) {
            this.setState({
                activeSession: tracks[activeTrack][activeSessionIndex - 1]
            });
        }  else if (e.keyCode === DOWN && activeSessionIndex < tracks[activeTrack].length - 1) {
            this.setState({
                activeSession: tracks[activeTrack][activeSessionIndex + 1]
            });
        }
    }

    updateTrack(i) {
        const { trackTypes } = this.state;
        const { tracks } = this.props;

        let activeTrack = trackTypes[i];

        this.setState({
            activeTrack: activeTrack,
            activeSession: tracks[activeTrack][0]
        });
    }

    render() {
        const { trackTypes, activeTrack, activeSession } = this.state;
        const { tracks } = this.props;

        return (
            <div style={ styles.base }>
                <Header />

                <main style={ container }>
                    <h1 style={ styles.heading }>
                        <SummitLogo style={ styles.summitLogo } />
                    </h1>
                    <div style={ styles.subheading }>Video Archive</div>

                    <Tabs
                        tabs={ trackTypes }
                        onChange={ (trackIndex) => this.updateTrack(trackIndex) }
                        activeTab={ trackTypes.indexOf(activeTrack) }
                    />

                    <div style={ styles.columns }>
                        <Sidebar
                            trackName={ activeTrack }
                            sessions={ tracks[activeTrack] }
                            activeSession={ activeSession }
                            onSessionChange={
                                (newSession) => this.setState({ activeSession: newSession })
                            }
                        />
                        <Article session={ activeSession } />
                    </div>

                    <Footer />
                </main>

                <Style rules={ globalStyles } />
            </div>
        );
    }
}

var styles = {
    base: {
        padding: '80px 0'
    },
    heading: {
        textAlign: 'center'
    },
    subheading: {
        textAlign: 'center',
        marginBottom: '60px',
        fontSize: '16px'
    },
    summitLogo: {
        display: 'inline-block',
        width: '430px',
        height: '53px',
        maxWidth: '100%'
    },
    columns: {
        display: 'flex',
        flexDirection: 'column',
        paddingTop: '40px',
        '@media (min-width: 800px)': {
            flexDirection: 'row',
        }
    }
};

export default Radium(App);
