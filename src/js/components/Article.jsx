'use strict';

import React from 'react';
import Radium from 'radium';
import Attribution from './Attribution';
import Speaker from './Speaker';
import sessionModel from '../models/sessionModel';
import { colors } from '../styles/styles';

const Article = ({ session }) => {
    const speakers = session.Speakers;

    return (
        <article style={ styles.base }>
            <h1 style={ styles.heading }>{ session.Title }</h1>
            
            { speakers && <Attribution speakers={ speakers } /> }

            <p>{ session.Description }</p>

            { speakers && (
                <h2>
                    About the speaker{ speakers.length > 1 ? 's' : '' }
                </h2>
            ) }

            { speakers && speakers.map((speaker, i) => (
                <Speaker speaker={ speaker } key={ i } />
            )) }
        </article>
    );
};

Article.propTypes = {
    session: sessionModel.isRequired
};

var styles = {
    base: {
        flexGrow: 1,
        paddingTop: '40px',
        '@media (min-width: 800px)': {
            paddingTop: 0,
            paddingLeft: '45px'
        }
    },
    heading: {
        margin: '0 0 20px',
        fontSize: '36px',
        color: colors.primary
    }
};

export default Radium(Article);
