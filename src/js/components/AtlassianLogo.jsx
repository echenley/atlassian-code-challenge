'use strict';

import React from 'react';
import Radium from 'radium';
import InlineSvg from 'svg-inline-react';

const AtlassianLogo = (props) => (
    <InlineSvg { ...props }
        src={ require('../../svg/atlassian-logo.svg') }
    />
);

export default Radium(AtlassianLogo);
