'use strict';

import React, { PropTypes } from 'react';
import Radium from 'radium';
import speakerModel from '../models/speakerModel';
import { colors } from '../styles/styles';

const Attribution = ({ speakers }) => (
    <p style={ styles.base }>
        { speakers.map((speaker) => (
            `${speaker.FirstName} ${speaker.LastName}, ${speaker.Company}`)
        ).join('; ') }
    </p>
);

Attribution.propTypes = {
    speakers: PropTypes.arrayOf(speakerModel).isRequired
};

var styles = {
    base: {
        margin: '14px 0 20px',
        color: colors.primary,
        fontWeight: 'bold'
    }
};

export default Radium(Attribution);
