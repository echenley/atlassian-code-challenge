'use strict';

import React from 'react';
import Radium from 'radium';
import { colors } from '../styles/styles';

const Footer = () => (
    <footer style={ styles.base }>
        <div style={ styles.row }>
            <a style={ styles.link } href="https://github.com/echenley">github</a>
            <span style={ styles.divider }>&middot;</span>
            <a style={ styles.link } href="https://twitter.com/echenley">twitter</a>
            <span style={ styles.divider }>&middot;</span>
            <a style={ styles.link } href="http://henleyedition.com">henleyedition.com</a>
            <span style={ styles.divider }>&middot;</span>
            <a style={ styles.link } href="mailto:evan.henley@gmail.com?subject=You're Hired!">evan.henley@gmail.com</a>
        </div>
        <div style={ [styles.row, styles.thanks] }>
            <span>Try the arrow keys. Thanks for reviewing!</span>
            <span style={ styles.smiley }>&#9786;</span>
        </div>
    </footer>
);

var styles = {
    base: {
        marginTop: '40px',
        paddingTop: '40px',
        borderTop: `1px solid ${colors.gray}`
    },
    row: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    divider: {
        margin: '0 10px'
    },
    link: {
        color: colors.primary,
        textDecoration: 'none'
    },
    thanks: {
        marginTop: '10px'
    },
    smiley: {
        marginLeft: '6px',
        fontSize: '16px'
    }
};

export default Radium(Footer);
