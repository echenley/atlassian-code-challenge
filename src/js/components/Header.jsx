'use strict';

import React from 'react';
import Radium from 'radium';
import AtlassianLogo from './AtlassianLogo';
import { colors, container } from '../styles/styles';

const Header = () => (
    <header style={ styles.header }>
        <div style={ [styles.headerInner, container] }>
            <AtlassianLogo style={ styles.logo } />
        </div>
    </header>
);

var styles = {
    header: {
        zIndex: 10,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        backgroundColor: colors.primary
    },
    headerInner: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        height: '50px'
    },
    logo: {
        display: 'block',
        width: '156px',
        height: '40px'
    }
};

export default Radium(Header);
