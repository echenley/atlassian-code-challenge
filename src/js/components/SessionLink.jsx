'use strict';

import React, { PropTypes } from 'react';
import Radium from 'radium';
import sessionModel from '../models/sessionModel';
import { colors } from '../styles/styles';

const SessionLink = ({ session, isActive, ...props }) => {
    const speaker = session.Speakers ? session.Speakers[0] : null;

    return (
        <a style={ isActive ? [styles.base, styles.active] : styles.base } { ...props }>
            <h2 style={ styles.heading }>{ session.Title }</h2>
            { speaker && (
                <div style={ styles.speaker }>
                    { speaker.FirstName } { speaker.LastName }, { speaker.Company }
                </div>
            ) }
        </a>
    );
};

SessionLink.propTypes = {
    session: sessionModel.isRequired,
    isActive: PropTypes.bool
};

var styles = {
    base: {
        display: 'block',
        padding: '10px',
        borderTop: `1px solid ${colors.gray}`,
        background: '#fff',
        cursor: 'pointer',
        ':hover': {
            background: colors.lightGray
        }
    },
    active: {
        background: colors.lightGray
    },
    heading: {
        margin: 0,
        fontSize: '14px',
        lineHeight: 1.3,
        color: colors.primary
    },
    speaker: {
        marginTop: '4px',
        fontSize: '12px',
        color: colors.darkGray
    }
};

export default Radium(SessionLink);
