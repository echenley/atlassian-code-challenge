'use strict';

import React, { PropTypes } from 'react';
import Radium from 'radium';
import SessionLink from './SessionLink';
import sessionModel from '../models/sessionModel';
import { colors } from '../styles/styles';

const Sidebar = ({ trackName, sessions, activeSession, onSessionChange }) => (
    <aside style={ styles.base }>
        <h1 style={ styles.heading }>{ trackName }</h1>

        <nav style={ styles.nav }>
            { sessions.map((session, i) => (
                <SessionLink 
                    session={ session }
                    isActive={ session === activeSession }
                    onClick={ () => onSessionChange(session) }
                    key={ i }
                />
            )) }
        </nav>
    </aside>
);

Sidebar.propTypes = {
    trackName: PropTypes.string.isRequired,
    sessions: PropTypes.arrayOf(sessionModel).isRequired,
    activeSession: sessionModel.isRequired,
    onSessionChange: PropTypes.func.isRequired
};

var styles = {
    base: {
        '@media (min-width: 800px)': {
            flexShrink: 0,
            width: '288px'
        }
    },
    nav: {
        borderBottom: `1px solid ${colors.gray}`
    },
    heading: {
        margin: '5px 0 20px',
        color: colors.primary
    }
};

export default Radium(Sidebar);
