'use strict';

import React from 'react';
import speakerModel from '../models/speakerModel';

const Content = ({ speaker }) => {
    const {
        FirstName,
        LastName,
        Company,
        Biography,
        Title
    } = speaker;

    return (
        <section>
            <p>
                <strong>{ FirstName } { LastName }</strong>
                { Title && `, ${Title}`}
                { Company && `, ${Company}`}
            </p>
            <p>{ Biography }</p>
        </section>
    );
};

Content.propTypes = {
    speaker: speakerModel.isRequired
};

export default Content;
