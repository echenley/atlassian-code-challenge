'use strict';

import React from 'react';
import Radium from 'radium';
import InlineSvg from 'svg-inline-react';

const SummitLogo = (props) => (
    <InlineSvg { ...props }
        src={ require('../../svg/summit-logo.svg') }
    />
);

export default Radium(SummitLogo);
