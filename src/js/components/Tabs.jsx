'use strict';

import React, { PropTypes } from 'react';
import Radium from 'radium';
import { colors } from '../styles/styles';

const Tabs = ({ tabs, onChange, activeTab }) => (
    <div style={ styles.base }>
        <div style={ styles.tabsInner }>
            { tabs.map((tab, i) => (
                <a
                    key={ i }
                    onClick={ () => onChange(i) }
                    style={ i === activeTab ? [styles.tab, styles.active] : styles.tab }
                >
                    { tab }
                </a>
            )) }
        </div>
    </div>
);

Tabs.propTypes = {
    tabs: PropTypes.arrayOf(PropTypes.string).isRequired,
    onChange: PropTypes.func.isRequired,
    activeTab: PropTypes.number.isRequired
};

var styles = {
    base: {
        display: 'flex',
        borderBottom: `1px solid ${colors.gray}`
    },
    tabsInner: {
        position: 'relative',
        top: '1px',
        display: 'flex',
        flexWrap: 'wrap',
        overflow: 'hidden',
        paddingRight: '1px',
        background: colors.gray,
        borderTop: `1px solid ${colors.gray}`,
        borderTopLeftRadius: '6px',
        borderTopRightRadius: '6px',
    },
    tab: {
        display: 'flex',
        alignItems: 'center',
        height: '40px',
        marginLeft: '1px',
        padding: '0 20px',
        background: colors.lightGray,
        borderBottom: `1px solid ${colors.gray}`,
        cursor: 'pointer',
        ':hover': {
            background: '#fff'
        }
    },
    active: {
        background: '#fff',
        borderBottom: '1px solid #fff'
    }
};

export default Radium(Tabs);
