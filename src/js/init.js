'use strict';

import 'isomorphic-fetch';

import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import App from './App';

function renderApp(data) {
    // eliminates 300ms tap delay on mobile
    // can be removed after React 1.0
    injectTapEventPlugin();

    ReactDOM.render(
        <App tracks={ data.tracks } />,
        document.getElementById('app-container')
    );
}

function organizeByTrack(res) {
    let sessions = res.Items;
    let tracks = {};

    sessions.forEach(session => {
        let trackTitle = session.Track.Title;

        if (!tracks[trackTitle]) {
            tracks[trackTitle] = [];
        }

        tracks[trackTitle].push(session);
    });

    return { tracks };
}

fetch('/static/sessions.json')
    .then((res) => res.json())
    .then(organizeByTrack)
    .then(renderApp);
