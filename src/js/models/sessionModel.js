'use strict';

import { PropTypes } from 'react';
import speakerModel from './speakerModel';

const { shape, string, arrayOf } = PropTypes;

export default shape({
    Description: string,
    Id: string,
    IsFeatured: string,
    LastModified: string,
    Mandatory: string,
    MetadataValues: arrayOf(shape({
        Details: shape({
            FieldType: string,
            Options: arrayOf(string),
            Title: string
        }),
        Value: string
    })),
    Published: string,
    Room: shape({
        Capacity: string,
        Name: string
    }),
    SessionType: shape({
        Name: string
    }),
    Speakers: arrayOf(speakerModel),
    TimeSlot: shape({
        EndTime: string,
        Label: string,
        StartTime: string
    }),
    Title: string,
    Track: shape({
        Description: string,
        Title: string
    })
});
