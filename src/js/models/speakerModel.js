'use strict';

import { PropTypes } from 'react';

const { shape, string, arrayOf } = PropTypes;

export default shape({
    AttendeeID: string,
    AvailableforMeeting: string,
    Biography: string,
    Company: string,
    EmailAddress: string,
    FirstName: string,
    ID: string,
    Industry: string,
    LastName: string,
    LastUpdated: string,
    Roles: arrayOf(string),
    Title: string
});
