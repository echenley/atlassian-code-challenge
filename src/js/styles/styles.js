'use strict';

export const globalStyles = {
    body: {
        margin: 0,
        fontFamily: '"Open Sans", sans-serif',
        fontSize: '14px',
        lineHeight: 1.5,
        WebkitFontSmoothing: 'antialiased',
        MozOsxFontSmoothing: 'grayscale',
    },
    'h1, h2, h3, h4': {
        lineHeight: 1.3
    },
    '*, *:before, *:after': {
        boxSizing: 'border-box'
    }
};

export const colors = {
    primary: '#005183',
    secondary: '#00b0e1',
    gray: '#ccc',
    lightGray: '#f5f5f5'
};

export const container = {
    width: '960px',
    maxWidth: '100%',
    margin: '0 auto',
    padding: '0 15px'
};
