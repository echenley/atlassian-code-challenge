var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var friendlyFormatter = require('eslint-friendly-formatter');

module.exports = {
    entry: [
        'webpack-dev-server/client?http://0.0.0.0:3000',
        'webpack/hot/only-dev-server',
        './src/js/init.js'
    ],
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'app.js'
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                include: path.join(__dirname, 'src'),
                loaders: ['react-hot', 'babel', 'eslint']
            },
            {
                test: /\.svg$/,
                loader: 'svg-inline'
            }
        ]
    },
    eslint: {
        formatter: friendlyFormatter,
    },
    devtool: 'source-map',
    devServer: {
        historyApiFallback: true
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.json']
    },
    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            title: 'billing-tool',
            template: './src/index.html',
            scriptFilename: 'app.js'
        }),
        new webpack.DefinePlugin({
            __DEVTOOLS__: true
        })
    ]
};