var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: [
        './src/js/init.js'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'app.min.js'
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                include: path.join(__dirname, 'src'),
                loader: 'babel'
            },
            {
                test: /\.svg$/,
                loader: 'svg-inline'
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.json']
    },
    plugins: [
        new webpack.NoErrorsPlugin(),
        new HtmlWebpackPlugin({
            title: 'billing-tool',
            template: './src/index.html',
            scriptFilename: 'app.min.js'
        }),
        new webpack.DefinePlugin({
            __DEVTOOLS__: false
        })
    ]
};